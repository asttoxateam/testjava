package ru.ant.access;


import ru.ant.staff.Accountant;
import ru.ant.staff.CheefAccountant;
import ru.ant.staff.Staff;

public class PaySalary {

    public static boolean accessPayer(Staff payer, Staff recipient){
        if ((payer instanceof CheefAccountant) &&
                (recipient instanceof Accountant)){
            return true;
        }
        else if(recipient instanceof Accountant){
            return false;
        }else{
            return true;
        }

    }

}
