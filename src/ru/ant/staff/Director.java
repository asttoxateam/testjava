package ru.ant.staff;

import ru.ant.staff.Staff;

public class Director extends Staff {
    private int mControlledEmployees;

    public Director(String name, int controlledEmployees) {
        super(name);
        this.mControlledEmployees = controlledEmployees;
    }

    public int getmControlledEmployees() {
        return mControlledEmployees;
    }

    public void setmControlledEmployees(int mControlledEmployees) {
        this.mControlledEmployees = mControlledEmployees;
    }

    public void setSalary(Staff staff, Double salary){
        staff.setSalary(salary);
    }


}
