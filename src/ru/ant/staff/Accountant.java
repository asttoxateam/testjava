package ru.ant.staff;

import ru.ant.access.PaySalary;

public  class Accountant extends Staff {

    public Accountant(String name) {
        super(name);
    }

    public Double paySalary(Staff staff){
        if(PaySalary.accessPayer(this,staff))
             return staff.getSalary();
        else return 0.0;
    }



}