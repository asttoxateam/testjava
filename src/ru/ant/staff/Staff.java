package ru.ant.staff;

public abstract class Staff{
    String mName;
   Double salary;

    public Staff (String name){
        this.mName = name;

    }

    public String getmName() {
        return mName;
    }



    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Double getSalary() {
        return salary;
    }

    public String toString(){
        return mName;
    }
}

