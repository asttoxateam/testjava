package ru.ant.company;


import ru.ant.staff.ProgectManager;
import ru.ant.staff.Programmer;
import ru.ant.staff.SaleManager;
import ru.ant.staff.TeamOfProgrammers;
import ru.ant.staff.Accountant;
import ru.ant.staff.CheefAccountant;
import ru.ant.staff.Director;

import java.util.ArrayList;

public class Company {



    public static void main(String[] args) {

        Director director = new Director("Ivanov", 20);;
        Programmer teamLeader1 = new Programmer( "Petrov"),
                prog1 = new Programmer( "BratPetrova"),
                prog2 = new Programmer( "Sidorov"),
                prog3 = new Programmer( "Sidorova"),
                prog4 = new Programmer( "BratSidorova");
        ArrayList<Programmer> programmersTeam = new ArrayList<>();
        programmersTeam.add(0, prog1);
        programmersTeam.add(1, prog2);

        TeamOfProgrammers teamProgr = new TeamOfProgrammers(programmersTeam, teamLeader1);


        SaleManager saleManager1 = new SaleManager("Frankentain"),
                saleManager2 = new SaleManager("KingOfSea"),
                saleManager3 = new SaleManager("QueenOfTree"),
                saleManager4 = new SaleManager("LordOfSwamp"),
                saleManager5 = new SaleManager("Knight");
        ProgectManager progectManager1 = new ProgectManager("");

        Accountant accountant1 = new Accountant("Mole1"),
                accountant2 = new Accountant("Mole2"),
                accountant3 = new Accountant("Mole3"),
                accountant4 = new Accountant("Mole4");
        CheefAccountant accountant5 = new CheefAccountant("MainMole");

        System.out.println("Директор "+director+" начислил зарплату:");
        director.setSalary(director,10000.0);
        System.out.println("1) себе - "+director.getSalary());
        director.setSalary(accountant5,9000.0);
        System.out.println("2) Главному бухгалтеру - "+accountant5.getSalary());
        director.setSalary(teamLeader1,8000.0);
        System.out.println("3) Рук. IT - "+teamLeader1.getSalary());
        director.setSalary(prog1,7000.0);
        System.out.println("4) prog1 - "+prog1.getSalary());
        director.setSalary(prog2,7000.0);
        System.out.println("5) prog2 - "+prog2.getSalary());

        director.setSalary(accountant1,6000.0);
        System.out.println("6) Бух 1 - "+accountant1.getSalary());
        director.setSalary(accountant2,6000.0);
        System.out.println("7) Бух 2 - "+accountant2.getSalary());

        System.out.println("Главбух "+accountant5+" выдал зп:");
        System.out.println("1) "+ accountant1 + " " + accountant5.paySalary(accountant1));
        System.out.println("2) "+ accountant2 + " " + accountant5.paySalary(accountant2));
        System.out.println("3) "+ director + " " + accountant5.paySalary(director));

        System.out.println("Бухгалтер "+accountant1+" выдал зп:");
        System.out.println("1) "+ teamLeader1 + " " + accountant5.paySalary(teamLeader1));
        System.out.println("2) "+ prog1 + " " + accountant5.paySalary(prog1));
        System.out.println("3) "+ prog2 + " " + accountant5.paySalary(prog2));

    }




}
